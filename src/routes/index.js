const routes = require('express').Router();

routes.use('/admin/users', require('./users'));
routes.use('/admin/login', require('./auth'));

module.exports = routes;
